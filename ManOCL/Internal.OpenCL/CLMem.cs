namespace ManOCL.Internal.OpenCL
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    internal struct CLMem
    {
        internal IntPtr Value;

        internal CLMem(IntPtr value)
        {
            this.Value = value;
        }
    }
}

