namespace ManOCL.Internal.OpenCL
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    internal struct CLContext
    {
        private static CLContext zero;
        internal static CLContext Zero
        {
            get
            {
                return zero;
            }
        }

        internal IntPtr Value;
    }
}

