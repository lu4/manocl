﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using ManOCL.Internal.OpenCL;

namespace ManOCL
{
    public abstract class Argument
    {
        internal static CLMemFlags GetMemAccessFlags(ArgumentBufferAccess access)
        {
            if (access == ArgumentBufferAccess.ReadWrite)
            {
                return CLMemFlags.ReadWrite;
            }
            else if (access == ArgumentBufferAccess.Read)
            {
                return CLMemFlags.ReadOnly;
            }
            else if (access == ArgumentBufferAccess.Write)
            {
                return CLMemFlags.WriteOnly;
            }
            else
            {
                throw new ArgumentException();
            }
        }

        internal abstract void SetAsKernelArgument(CLKernel kernel, Int32 index);
    }
}
