﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using ManOCL.Internal.OpenCL;

namespace ManOCL
{
    public partial class Platforms : IEnumerable<Platform>
    {
        public static Platforms Create()
        {
            Int32 platformsCount = 0;
            CLPlatformID[] platforms;

            OpenCLError.Validate(OpenCLDriver.clGetPlatformIDs(0, null, ref platformsCount));

            platforms = new CLPlatformID[platformsCount];

            OpenCLError.Validate(OpenCLDriver.clGetPlatformIDs(platformsCount, platforms, ref platformsCount));

            if (platforms.Length != platformsCount)
            {
                throw new InvalidOperationException(Resources.Amount_of_platforms_changed_in_between_the_calls_to_clGetPlatformIDs);
            }

            return new Platforms(platforms);
        }
        public static Platforms GetUniqueOpenCLPlatforms(Devices devices)
        {
            Dictionary<CLPlatformID, Device> platforms = new Dictionary<CLPlatformID, Device>();

            foreach (Device device in devices)
            {
                if (!platforms.ContainsKey(device.PlatformID))
                {
                    platforms.Add(device.PlatformID, device);
                }
            }

            CLPlatformID[] result = new CLPlatformID[platforms.Count];

            platforms.Keys.CopyTo(result, 0);

            return new Platforms(result);
        }

        private Platform[] platforms;

        internal Platforms(CLPlatformID[] clPlatforms)
        {
            this.platforms = new Platform[clPlatforms.Length];

            for (Int32 i = 0; i < platforms.Length; i++)
            {
                platforms[i] = new Platform(clPlatforms[i]);
            }
        }

        public Int32 Count
        {
            get
            {
                return platforms.Length;
            }
        }

        public Platform this[Int32 index]
        {
            get
            {
                return platforms[index];
            }
        }

        public IEnumerator<Platform> GetEnumerator()
        {
            foreach (Platform platform in platforms)
            {
                yield return platform;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            foreach (Platform platform in platforms)
            {
                yield return platform;
            }
        }

        internal String ToIdentedString(Int32 identOrder, Int32 identSize)
        {
            String ident = new String(' ', identSize * identOrder);

            StringBuilder sb = new StringBuilder();

            sb.AppendLine(ident + "Platforms");
            sb.AppendLine(ident + "{");

            foreach (Platform platform in platforms)
            {
                sb.AppendLine(platform.ToIdentedString(identOrder + 1, identSize));
            }

            sb.AppendLine(ident + "}");

            return sb.ToString();
        }

        public String ToToIdentedString(Int32 identSize)
        {
            return ToIdentedString(0, identSize);
        }

        public override string ToString()
        {
            return ToIdentedString(0, Globals.IdentSize);
        }
 
    }
}