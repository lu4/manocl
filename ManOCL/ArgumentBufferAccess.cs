﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManOCL
{
    public enum ArgumentBufferAccess { None = 0, Read = 1, Write = 2, ReadWrite = 3 }
}
