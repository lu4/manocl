﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManOCL
{
    using Internal.OpenCL;

    public class KernelArguments
    {
        private CLKernel clKernel;
        private Argument[] arguments;

        internal KernelArguments(CLKernel clKernel)
        {
            this.clKernel = clKernel;
            this.arguments = new Argument[Kernel.GetKernelInfo<Int32>(clKernel, CLKernelInfo.NumArgs)];
        }

        public Int32 Count
        {
            get
            {
                return arguments.Length;
            }
        }

        public Argument this[Int32 index]
        {
            get
            {
                return arguments[index];
            }
            set
            {
                if (arguments[index] != value)
                {
                    arguments[index] = value;

                    value.SetAsKernelArgument(clKernel, index);
                }
            }
        }
    }
}
