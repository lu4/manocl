﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManOCL
{
    public enum ProgramBuildStatus
    {
        Error = -2,
        InProgress = -3,
        None = -1,
        Success = 0
    }
}
