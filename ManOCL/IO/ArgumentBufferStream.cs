﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using ManOCL;

namespace ManOCL.IO
{
    public class ArgumentBufferStream : Stream
    {
        private Int64 _Position = 0;

        public ArgumentBuffer DeviceBuffer { get; private set; }

        public CommandQueue CommandQueue { get; private set; }

        public ArgumentBufferStream(CommandQueue commandQueue, ArgumentBuffer deviceBuffer)
        {
            this.DeviceBuffer = deviceBuffer;
            this.CommandQueue = commandQueue;
        }

        public override bool CanRead
        {
            get
            {
                return true;
            }
        }
        public override bool CanSeek
        {
            get
            {
                return true;
            }
        }
        public override bool CanWrite
        {
            get
            {
                return true;
            }
        }

        public override void Flush()
        {
        }
        public override Int64 Length
        {
            get
            {
                return DeviceBuffer.Size;
            }
        }
        public override Int64 Position
        {
            get
            {
                return _Position;
            }
            set
            {
                _Position = value;
            }
        }

        public override Int32 Read(byte[] buffer, Int32 offset, Int32 count)
        {
            Int64 newPosition = _Position + count;
            Int64 overflow = Length - newPosition;

            count += (Int32)(overflow < 0 ? overflow : 0);

            DeviceBuffer.TransferTo(CommandQueue, buffer, offset, (Int32)_Position, count, null);

            _Position += count;

            return count;
        }

        public override Int64 Seek(Int64 offset, SeekOrigin origin)
        {
            if (origin == SeekOrigin.Begin)
            {
                return _Position = offset;
            }
            else if (origin == SeekOrigin.Current)
            {
                return _Position += offset;
            }
            else if (origin == SeekOrigin.End)
            {
                return _Position = Length + offset;
            }
            else
            {
                throw new ArgumentException(Resources.Invalid_SeekOrigin);
            }
        }

        public override void SetLength(Int64 value)
        {
            throw new InvalidOperationException(Resources.DeviceBufferStream_SetLength_method_can_t_change_DeviceBuffer_size_because_it_is_a_fixed_size_object);
        }

        public override void Write(byte[] buffer, Int32 offset, Int32 count)
        {
            DeviceBuffer.TransferFrom(CommandQueue, buffer, offset, _Position, count, null);

            _Position += count;
        }
    }
}