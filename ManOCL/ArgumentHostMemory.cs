﻿using System;
using System.Collections.Generic;
using System.Text;
using ManOCL.Internal.OpenCL;
using System.Runtime.InteropServices;
using ManOCL.Internal;


namespace ManOCL
{
    public class ArgumentHostMemory : ArgumentBuffer
    {
        internal ArgumentHostMemory(CLMem clMem, Context context, Int32 size, ArgumentBufferAccess access)
            :base(clMem, size, context, access)
        {
        }

        public static ArgumentHostMemory Use(Context context, Array data, ArgumentBufferAccess access)
        {
            SizeT size = Marshal.SizeOf(data.GetType().GetElementType()) * data.Length;

            CLMem clMem = CreateCLMem(data, size, GetMemAccessFlags(access) | CLMemFlags.UseHostPtr, context, access);

            return new ArgumentHostMemory(clMem, context, size, access);
        }

        public static ArgumentHostMemory SizeCreate(Context context, ArgumentBufferAccess access, Int32 size)
        {
            CLMem clMem = CreateCLMem<Array>(null, size, GetMemAccessFlags(access) | CLMemFlags.AllocHostPtr, context, access);

            return new ArgumentHostMemory(clMem, context, size, access);
        }
        public static ArgumentHostMemory ArrayCreate(Context context, ArgumentBufferAccess access, Array data)
        {
            SizeT size = Marshal.SizeOf(data.GetType().GetElementType()) * data.Length;

            CLMem clMem = CreateCLMem(data, size, GetMemAccessFlags(access) | CLMemFlags.AllocHostPtr | CLMemFlags.CopyHostPtr, context, access);

            return new ArgumentHostMemory(clMem, context, size, access);
        }
        public static ArgumentHostMemory StructCreate<T>(Context context, ArgumentBufferAccess access, T data) where T : struct
        {
            SizeT size = Marshal.SizeOf(data.GetType());

            CLMem clMem = CreateCLMem<T>(data, size, GetMemAccessFlags(access) | CLMemFlags.AllocHostPtr | CLMemFlags.CopyHostPtr, context, access);

            return new ArgumentHostMemory(clMem, context, size, access);
        }
    }
}
