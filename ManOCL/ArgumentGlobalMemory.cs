﻿using System;
using System.IO;
using System.Runtime.InteropServices;

using ManOCL.Internal.OpenCL;
using ManOCL.Internal;


namespace ManOCL
{
    public class ArgumentGlobalMemory : ArgumentBuffer
    {
        internal ArgumentGlobalMemory(CLMem clMem, Context context, Int32 size, ArgumentBufferAccess access)
            :base(clMem, size, context, access)
        {
        }

        public static ArgumentGlobalMemory SizeCreate(Context context, ArgumentBufferAccess access, Int32 size)
        {
            CLMem clMem = CreateCLMem<Array>(null, size, GetMemAccessFlags(access), context, access);

            return new ArgumentGlobalMemory(clMem, context, size, access);
        }
        public static ArgumentGlobalMemory ArrayCreate(Context context, ArgumentBufferAccess access, Array data)
        {
            SizeT size = Marshal.SizeOf(data.GetType().GetElementType()) * data.Length;

            CLMem clMem = CreateCLMem(data, size, GetMemAccessFlags(access) | CLMemFlags.CopyHostPtr, context, access);

            return new ArgumentGlobalMemory(clMem, context, size, access);
        }
        public static ArgumentGlobalMemory StructCreate<T>(Context context, ArgumentBufferAccess access, T data) where T : struct
        {
            SizeT size = Marshal.SizeOf(data.GetType());

            CLMem clMem = CreateCLMem(data, size, GetMemAccessFlags(access) | CLMemFlags.CopyHostPtr, context, access);

            return new ArgumentGlobalMemory(clMem, context, size, access);
        }
    }
}