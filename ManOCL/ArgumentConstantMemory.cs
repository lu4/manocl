﻿using System;
using System.Collections.Generic;
using System.Text;
using ManOCL.Internal.OpenCL;
using System.Runtime.InteropServices;
using ManOCL.Internal;


namespace ManOCL
{
    public class ArgumentConstantMemory : ArgumentBuffer
    {
        internal ArgumentConstantMemory(CLMem clMem, Context context, Int32 size)
            :base(clMem, size, context, ArgumentBufferAccess.Read)
        {
        }

        public static ArgumentConstantMemory SizeCreate(Context context, Int32 size)
        {
            CLMem clMem = CreateCLMem<Array>(null, size, CLMemFlags.ReadOnly, context, ArgumentBufferAccess.Read);

            return new ArgumentConstantMemory(clMem, context, size);
        }
        public static ArgumentConstantMemory ArrayCreate(Context context, Array data)
        {
            SizeT size = Marshal.SizeOf(data.GetType().GetElementType()) * data.Length;

            CLMem clMem = CreateCLMem<Array>
            (
                null, size,
                CLMemFlags.ReadOnly | CLMemFlags.CopyHostPtr,
                context,
                ArgumentBufferAccess.Read
            );

            return new ArgumentConstantMemory(clMem, context, size);
        }
        public static ArgumentConstantMemory StructCreate<T>(Context context, T data) where T : struct
        {
            SizeT size = Marshal.SizeOf(data.GetType());

            CLMem clMem = CreateCLMem<Array>
            (
                null, size,
                CLMemFlags.ReadOnly | CLMemFlags.CopyHostPtr,
                context,
                ArgumentBufferAccess.Read
            );

            return new ArgumentConstantMemory(clMem, context, size);
        }
    }
}
