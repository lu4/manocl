﻿using System;

using ManOCL.Internal.OpenCL;

namespace ManOCL
{
    public class Event : IDisposable
    {
        private Boolean disposed;
        
        internal CLEvent CLEvent { get; private set; }

        internal Event(CLEvent clEvent)
        {
            this.CLEvent = clEvent;
        }
        
        #region IDisposable implementation
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        
        protected virtual void Dispose(Boolean disposing)
        {
            if (!disposed)
            {
                disposed = true;

                OpenCLDriver.clReleaseEvent(CLEvent);
            }
        }
        #endregion
        
        ~Event()
        {
            Dispose(false);
        }
    }
}
