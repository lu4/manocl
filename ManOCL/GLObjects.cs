﻿using System;
using System.Collections.Generic;
using System.Text;
using ManOCL.Internal.OpenCL;
using ManOCL.Internal.OpenCL.OpenGL;

namespace ManOCL
{
    public class GLObjects
    {
        private Boolean acquired;
        private CLMem[] glObjects;

        public Event Acquire(CommandQueue commandQueue)
        {
            return Acquire(commandQueue, Events.None);
        }
        public Event Acquire(CommandQueue commandQueue, Events eventWaitList)
        {
            CLEvent e = new CLEvent();

            OpenCLError.Validate(OpenCLGLDriver.clEnqueueAcquireGLObjects(commandQueue.CLCommandQueue, glObjects.Length, glObjects, eventWaitList.Count, eventWaitList.OpenCLEventArray, ref e));

            acquired = true;

            return new Event(e);
        }

        public Event Release(CommandQueue commandQueue)
        {
            return Release(commandQueue, Events.None);
        }
        public Event Release(CommandQueue commandQueue, Events eventWaitList)
        {
            CLEvent e = new CLEvent();

            OpenCLError.Validate(OpenCLGLDriver.clEnqueueReleaseGLObjects(commandQueue.CLCommandQueue, glObjects.Length, glObjects, eventWaitList.Count, eventWaitList.OpenCLEventArray, ref e));

            acquired = false;

            return new Event(e);
        }
    }
}
