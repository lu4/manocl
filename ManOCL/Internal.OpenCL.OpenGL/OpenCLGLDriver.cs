namespace ManOCL.Internal.OpenCL.OpenGL
{
    using ManOCL.Internal.OpenCL;
    using System;
    using System.Runtime.InteropServices;

    /// <summary>
    /// This class provides the driver interface for OpenGL interoperability
    /// with OpenCL standard.
    /// </summary>
    internal class OpenCLGLDriver
    {
        [DllImport("OpenCL")]
        internal static extern CLError clCreateFromGLBuffer(CLContext context, CLMemFlags flags, Int32 bufobj, ref CLError errcode_ret);
        [DllImport("OpenCL")]
        internal static extern CLError clCreateFromGLRenderbuffer(CLContext context, CLMemFlags flags, Int32 renderbuffer, ref CLError errcode_ret);
        [DllImport("OpenCL")]
        internal static extern CLMem clCreateFromGLTexture2D(CLContext context, CLMemFlags flags, Int32 target, Int32 miplevel, UInt32 texture, ref CLError errcode_ret);
        [DllImport("OpenCL")]
        internal static extern CLMem clCreateFromGLTexture3D(CLContext context, CLMemFlags flags, Int32 target, Int32 miplevel, UInt32 texture, ref CLError errcode_ret);

        [DllImport("OpenCL")]
        internal static extern CLError clEnqueueAcquireGLObjects(CLCommandQueue command_queue, Int32 num_objects, [In] CLMem[] mem_objects, Int32 num_events_in_wait_list, [In] CLEvent[] event_wait_list, ref CLEvent e);

        [DllImport("OpenCL")]
        internal static extern CLError clEnqueueReleaseGLObjects(CLCommandQueue command_queue, Int32 num_objects, [In] CLMem[] mem_objects, Int32 num_events_in_wait_list, [In] CLEvent[] event_wait_list, ref CLEvent e);

        [DllImport("OpenCL")]
        internal static extern CLError clGetGLObjectInfo(CLMem memobj, ref Int32 gl_object_type, ref Int32 gl_object_name);
        [DllImport("OpenCL")]
        internal static extern CLError clGetGLTextureInfo(CLMem memobj, Int32 param_name, SizeT param_value_size, IntPtr param_value, ref SizeT param_value_size_ret);
    }
}

