﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace ManOCL
{
    [StructLayout(LayoutKind.Sequential)]
    public struct ImageFormat
    {
        private ChannelOrder channelOrder;
        private ChannelType channelType;

        public ImageFormat(ChannelType channelType, ChannelOrder channelOrder)
        {
            this.channelType = channelType;
            this.channelOrder = channelOrder;
        }

        public ChannelType ChannelType
        {
            get
            {
                return channelType;
            }
        }
        public ChannelOrder ChannelOrder
        {
            get
            {
                return channelOrder;
            }
        }
    }
}
