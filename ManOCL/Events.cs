﻿using System;
using System.Collections.Generic;
using System.Text;
using ManOCL.Internal.OpenCL;
using System.Collections;

namespace ManOCL
{
    public partial class Events : IDisposable, IEnumerable<Event>
    {
        internal Event[] EventArray { get; private set; }
        internal CLEvent[] OpenCLEventArray { get; private set; }

        internal static Event[] GetEventArray(CLEvent[] clEvents)
        {
            if (clEvents != null && clEvents.Length != 0)
            {
                Event[] events = new Event[clEvents.Length];

                for (UInt32 i = 0; i < clEvents.Length; i++)
                {
                    events[i] = new Event(clEvents[i]);
                }

                return events;
            }
            else
            {
                return null;
            }
        }
        internal static CLEvent[] GetOpenCLEventArray(Event[] events)
        {
            if (events != null && events.Length != 0)
            {
                CLEvent[] clEvents = new CLEvent[events.Length];
    
                for (UInt32 i = 0; i < events.Length; i++)
                {
                    clEvents[i] = events[i].CLEvent;
                }
    
                return clEvents;
            }
            else
            {
                return null;
            }
        }

        internal Events(CLEvent[] clEvents, Event[] events, Int32 count)
        {
            this.Count = count;
            
            this.EventArray = events;
            this.OpenCLEventArray = clEvents;
        }
        
        internal Events(CLEvent[] clEvents)
            // No clone is needed since this is internal side (which is stupid user-safe)
            : this(clEvents, GetEventArray(clEvents), clEvents == null ? 0 : clEvents.Length)
        {
        }

        private Boolean disposed;

        public Events(params Event[] eventArray)
            : this(GetOpenCLEventArray(eventArray), eventArray == null ? null : (Event[])eventArray.Clone(), eventArray == null ? 0 : eventArray.Length)
        {
        }

        public Int32 Count { get; private set; }

        public Event this[Int32 index]
        {
            get
            {
                return EventArray[index];
            }
        }

        #region public static Events None { get; }
        private static Events _Empty;
        public static Events None
        {
            get
            {
                if (_Empty == null)
                {
                    _Empty = new Events(null, null, 0);
                }
                
                return _Empty;
            }
        }
        #endregion

        // IEnumerable

        #region IEnumerable implementation
        public IEnumerator<Event> GetEnumerator()
        {
            foreach (Event evt in EventArray)
            {
                yield return evt;
            }
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            foreach (Event evt in EventArray)
            {
                yield return evt;
            }
        }
        #endregion

        #region IDisposable implementation
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                for (Int32 i = 0; i < EventArray.Length; i++)
                {
                    EventArray[i].Dispose();
                }

                disposed = true;
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        ~Events()
        {
            Dispose(false);
        }
    }
}
