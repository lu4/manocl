﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using ManOCL.Internal.OpenCL;
using ManOCL.Internal;
using System.Text;


namespace ManOCL
{
    public partial class Program : IDisposable
    {
        /* Static members */
        public static Program Create(Context context, Devices devices, params String[] sources)
        {
            SizeT[] lengths = new SizeT[sources.Length];

            for (Int32 i = 0; i < lengths.Length; i++)
            {
                lengths[i] = new SizeT(sources[0].Length);
            }

            CLError error = CLError.None;

            CLProgram clProgram = OpenCLDriver.clCreateProgramWithSource(context.CLContext, sources.Length, sources, lengths, ref error);

            OpenCLError.Validate(error);

            return new Program(clProgram, sources.Clone() as String[], context, devices);
        }

        /* Instance members - private */
        private Boolean disposed;
        private String[] sources;

        private void NotifyBuildAsyncComplete(CLProgram program, IntPtr user_data)
        {
            if (BuildAsyncComplete != null)
            {
                BuildAsyncComplete(this, null);
            }
        }

        private T GetProgramBuildInfo<T>(Device device, CLProgramBuildInfo programBuildInfo)
            where T : struct
        {
            SizeT bufferSize = SizeT.Zero;

            OpenCLError.Validate(OpenCLDriver.clGetProgramBuildInfo(CLProgram, device.CLDeviceID, programBuildInfo, bufferSize, IntPtr.Zero, ref bufferSize));

            Byte[] buffer = new Byte[bufferSize];

            GCHandle bufferHandle = GCHandle.Alloc(buffer, GCHandleType.Pinned);

            try
            {
                OpenCLError.Validate(OpenCLDriver.clGetProgramBuildInfo(CLProgram, device.CLDeviceID, programBuildInfo, bufferSize, bufferHandle.AddrOfPinnedObject(), ref bufferSize));

                return (T)Marshal.PtrToStructure(bufferHandle.AddrOfPinnedObject(), typeof(T));
            }
            finally
            {
                bufferHandle.Free();
            }

        }
        private Byte[] GetProgramBuildInfoBuffer(Device device, CLProgramBuildInfo programBuildInfo)
        {
            SizeT bufferSize = SizeT.Zero;

            OpenCLError.Validate(OpenCLDriver.clGetProgramBuildInfo(CLProgram, device.CLDeviceID, programBuildInfo, bufferSize, IntPtr.Zero, ref bufferSize));

            Byte[] buffer = new Byte[bufferSize];

            GCHandle bufferHandle = GCHandle.Alloc(buffer, GCHandleType.Pinned);

            try
            {
                OpenCLError.Validate(OpenCLDriver.clGetProgramBuildInfo(CLProgram, device.CLDeviceID, programBuildInfo, bufferSize, bufferHandle.AddrOfPinnedObject(), ref bufferSize));
            }
            finally
            {
                bufferHandle.Free();
            }

            return buffer;
        }
        private String GetProgramBuildInfoString(Device device, CLProgramBuildInfo programBuildInfo)
        {
            byte[] buffer = GetProgramBuildInfoBuffer(device, programBuildInfo);

            Int32 count = Array.IndexOf<byte>(buffer, 0);

            return System.Text.ASCIIEncoding.ASCII.GetString(buffer, 0, count < 0 ? buffer.Length : count).Trim();
        }

        /* Instance members - internal */
        internal Program(CLProgram clProgram, String[] sources, Context context, Devices devices)
        {
            this.Context = context;
            this.Devices = devices;
            this.CLProgram = clProgram;
            this.sources = sources;
        }
        internal CLProgram CLProgram { get; private set; }

        /* Instance members - public */
        public Boolean Built { get; private set; }

        public Context Context { get; private set; }
        public Devices Devices { get; private set; }

        public IEnumerable<String> Sources
        {
            get
            {
                foreach (String source in sources)
                {
                    yield return source;
                }
            }
        }

        public Boolean Build()
        {
            return Build("");
        }
        public Boolean Build(String buildOptions)
        {
            return Built = OpenCLDriver.clBuildProgram(CLProgram, Devices.Count, Devices.OpenCLDeviceArray, buildOptions, null, IntPtr.Zero) == CLError.None;
        }

        public void BuildAsync()
        {
            BuildAsync("");
        }
        public void BuildAsync(String buildOptions)
        {
            OpenCLError.Validate(OpenCLDriver.clBuildProgram(CLProgram, Devices.Count, Devices.OpenCLDeviceArray, buildOptions, NotifyBuildAsyncComplete, IntPtr.Zero));
        }

        public String GetBuildLog(Device device)
        {
            return GetProgramBuildInfoString(device, CLProgramBuildInfo.Log);
        }
        public String GetBuildOptions(Device device)
        {
            return GetProgramBuildInfoString(device, CLProgramBuildInfo.Options);
        }
        public ProgramBuildStatus GetBuildStatus(Device device)
        {
            return GetProgramBuildInfo<ProgramBuildStatus>(device, CLProgramBuildInfo.Status);
        }

        public event EventHandler BuildAsyncComplete;

        #region IDisposable members
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                OpenCLError.Validate(OpenCLDriver.clReleaseProgram(CLProgram));

                disposed = true;
            }
        }
        #endregion

        ~Program()
        {
            Dispose(false);
        }
    }
}