﻿using System;
using System.Collections.Generic;
using System.Text;

using ManOCL.Internal.OpenCL;

namespace ManOCL
{
    public class CommandQueue : IDisposable
    {
        private Boolean disposed;

        internal CLCommandQueue CLCommandQueue { get; private set; }

        internal CommandQueue(CLCommandQueue clCommandQueue, Context context, Device device, CommandQueueProperties commandQueueProperties)
        {
            this.Device = device;
            this.Context = context;
            this.CLCommandQueue = clCommandQueue;
            this.Properties = commandQueueProperties;
        }

        public static CommandQueue Create(Context context, Device device, CommandQueueProperties properties)
        {
            CLError error = CLError.None;

            CLCommandQueue clCommandQueue = OpenCLDriver.clCreateCommandQueue(context.CLContext, device.CLDeviceID, (ManOCL.Internal.OpenCL.CLCommandQueueProperties)properties, ref error);

            OpenCLError.Validate(error);

            return new CommandQueue(clCommandQueue, context, device, properties);
        }

        public Device Device { get; private set; }
        public Context Context { get; private set; }

        public CommandQueueProperties Properties { get; private set; }

        public void Finish()
        {
            OpenCLError.Validate(OpenCLDriver.clFinish(CLCommandQueue));
        }

        #region IDisposable members
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                OpenCLError.Validate(OpenCLDriver.clReleaseCommandQueue(CLCommandQueue));

                disposed = true;
            }
        }
        #endregion

        ~CommandQueue()
        {
            Dispose(false);
        }
    }
}