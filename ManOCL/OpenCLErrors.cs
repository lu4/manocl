using System;
using ManOCL.Internal.OpenCL;
using System.Runtime.InteropServices;
using ManOCL;
using ManOCL.Internal;


namespace ManOCL
{
    public class OpenCLException : Exception
    {
        public OpenCLException()
        {
        }

        public OpenCLException(String message)
            : base (message)
        {
        }
    }
    
    internal class OpenCLError : OpenCLException
    {
        internal CLError ErrorCode { get; private set; }

        internal OpenCLError(CLError errorCode)
        {
            this.ErrorCode = errorCode;
        }

        internal String BaseExceptionString
        {
            get
            {
                return base.ToString();
            }
        }

        public override String ToString()
        {
            return String.Format("OpenCL error occured, error code = {0}, additional info:\r\n{1}", ErrorCode, BaseExceptionString);
        }
        
        internal static void Validate(CLError error)
        {
            if (error != CLError.None) throw new OpenCLError(error);
        }

        internal static void Validate(Int32 error)
        {
            if ((CLError)error != CLError.None) throw new OpenCLError((CLError)error);
        }
    }

    internal class OpenCLBuildException : OpenCLException
    {
        public OpenCLBuildException()
            : base()
        {
        }

        public OpenCLBuildException(String message)
            : base(message)
        {
        }
    }
}

