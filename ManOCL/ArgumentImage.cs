using System;
using System.Runtime.InteropServices;

using ManOCL;
using ManOCL.Internal.OpenCL;
using ManOCL.Internal.OpenCL.OpenGL;
using ManOCL.Internal;

namespace ManOCL
{
    public class ArgumentImage2D : Argument, IDisposable
    {
        private CLMem clMem;
        private Boolean disposed;

        internal ArgumentImage2D(CLMem clMem, Context context, ArgumentBufferAccess access)
        {
            this.clMem = clMem;
            this.Access = access;
            this.Context = context;

            this.Width = GetImageInfo<Int32>(CLImageInfo.Width);
            this.Heigth = GetImageInfo<Int32>(CLImageInfo.Height);
            this.RowPitch = GetImageInfo<Int32>(CLImageInfo.RowPitch);
            this.Format = GetImageInfo<ImageFormat>(CLImageInfo.Format);
            this.ElementSize = GetImageInfo<Int32>(CLImageInfo.ElementSize);
        }

        //public static ArgumentImage2D Texture2DCreate(Context context, UInt32 texture, ArgumentBufferAccess access)
        //{
        //    return Texture2DCreate(context, texture, access, 0);
        //}
        //public static ArgumentImage2D Texture2DCreate(Context context, UInt32 texture, ArgumentBufferAccess access, Int32 mipLevel)
        //{
        //    CLError error = CLError.None;

        //    CLMem clMem = ManOCL.Internal.OpenCL.OpenGL.OpenCLGLDriver.clCreateFromGLTexture2D(context.CLContext, GetMemAccessFlags(access), 3553 /* GL_TEXTURE_2D */, mipLevel, texture, ref error);

        //    OpenCLError.Validate(error);

        //    ArgumentImage2D result = new ArgumentImage2D(clMem, context);

        //    return result;
        //}

        //public static ArgumentImage2D Create(Context context, )
        //{
        //    OpenCLDriver.clCreateImage2D(context, 
        //}

        private String GetImageInfoString(CLImageInfo imageInfo)
        {
            byte[] buffer = GetImageInfoBuffer(imageInfo);

            Int32 count = Array.IndexOf<byte>(buffer, 0);

            return System.Text.ASCIIEncoding.ASCII.GetString(buffer, 0, count < 0 ? buffer.Length : count).Trim();
        }
        private Byte[] GetImageInfoBuffer(CLImageInfo imageInfo)
        {
            SizeT bufferSize = SizeT.Zero;

            OpenCLError.Validate(OpenCLDriver.clGetImageInfo(clMem, imageInfo, SizeT.Zero, IntPtr.Zero, ref bufferSize));

            Byte[] buffer = new Byte[bufferSize];

            GCHandle bufferHandle = GCHandle.Alloc(buffer, GCHandleType.Pinned);

            try
            {
                OpenCLError.Validate(OpenCLDriver.clGetImageInfo(clMem, imageInfo, bufferSize, bufferHandle.AddrOfPinnedObject(), ref bufferSize));
            }
            finally
            {
                bufferHandle.Free();
            }

            return buffer;
        }
        private T GetImageInfo<T>(CLImageInfo imageInfo) where T : struct
        {
            Byte[] buffer = GetImageInfoBuffer(imageInfo);

            GCHandle bufferHandle = GCHandle.Alloc(buffer, GCHandleType.Pinned);

            try
            {
                return (T)Marshal.PtrToStructure(bufferHandle.AddrOfPinnedObject(), typeof(T));
            }
            finally
            {
                bufferHandle.Free();
            }
        }

        public Int32 Width { get; private set; }
        public Int32 Heigth { get; private set; }
        public Int32 RowPitch { get; private set; }
        public Int32 ElementSize { get; private set; }
        public ImageFormat Format { get; private set; }
        public ArgumentBufferAccess Access { get; private set; }

        public Context Context { get; private set; }

        internal override void SetAsKernelArgument(CLKernel kernel, Int32 index)
        {
            OpenCLError.Validate(OpenCLDriver.clSetKernelArg(kernel, index, new SizeT(IntPtr.Size), ref clMem));
        }

        #region IDisposable members
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                OpenCLError.Validate(OpenCLDriver.clReleaseMemObject(clMem));

                disposed = true;
            }
        }
        #endregion

        ~ArgumentImage2D()
        {
            Dispose(false);
        }
    }
}