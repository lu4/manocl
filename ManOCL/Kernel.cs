﻿using System;
using ManOCL.Internal.OpenCL;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using ManOCL.Internal;
using System.Text;


namespace ManOCL
{
    public partial class Kernel : IDisposable
    {
        /* Private members */
        private bool disposed;

        /* Internal members */
        internal CLKernel CLKernel { get; private set; }

        internal Kernel(CLKernel clKernel, Program program)
            : this(clKernel, program, GetKernelInfoString(clKernel, CLKernelInfo.FunctionName))
        {
        }

        internal Kernel(CLKernel clKernel, Program program, String name)
        {
            this.CLKernel = clKernel;

            this.Name = name;
            this.Program = program;
            this.Context = program.Context;

            this.Arguments = new KernelArguments(clKernel);
        }

        internal Event ExecuteInternal(CommandQueue commandQueue, SizeT[] globalWorkSize)
        {
            return ExecuteInternal(commandQueue, globalWorkSize, null, Events.None, null);
        }
        internal Event ExecuteInternal(CommandQueue commandQueue, SizeT[] globalWorkSize, SizeT[] localWorkSize)
        {
            return ExecuteInternal(commandQueue, globalWorkSize, localWorkSize, Events.None, null);
        }
        internal Event ExecuteInternal(CommandQueue commandQueue, SizeT[] globalWorkSize, SizeT[] localWorkSize, Events eventWaitList)
        {
            return ExecuteInternal(commandQueue, globalWorkSize, localWorkSize, eventWaitList, null);
        }
        internal Event ExecuteInternal(CommandQueue commandQueue, SizeT[] globalWorkSize, SizeT[] localWorkSize, Events eventWaitList, SizeT[] globalWorkOffset)
        {
            if (localWorkSize == null || globalWorkSize.Length == localWorkSize.Length)
            {
                CLEvent e = new CLEvent();

                OpenCLError.Validate(OpenCLDriver.clEnqueueNDRangeKernel(commandQueue.CLCommandQueue, CLKernel, globalWorkSize.Length, globalWorkOffset, globalWorkSize, localWorkSize, eventWaitList.Count, eventWaitList.OpenCLEventArray, ref e));

                return new Event(e);
            }
            else
            {
                throw new RankException(Resources.LocalWorkSize_and_GlobalWorkSize_dimensions_do_not_agree);
            }
        }

        public String Name { get; private set; }

        public Context Context { get; private set; }
        public Program Program { get; private set; }
        public KernelArguments Arguments { get; private set; }

        public Event Execute(CommandQueue commandQueue, Int32 globalWorkSize)
        {
            return Execute(commandQueue, new Int32[] { globalWorkSize });
        }
        public Event Execute(CommandQueue commandQueue, Int32 globalWorkSize, Int32 localWorkSize)
        {
            return Execute(commandQueue, new Int32[] { globalWorkSize }, new Int32[] { localWorkSize });
        }
        public Event Execute(CommandQueue commandQueue, Int32[] globalWorkSize)
        {
            return ExecuteInternal(commandQueue, Convert(globalWorkSize));
        }
        public Event Execute(CommandQueue commandQueue, Int32[] globalWorkSize, Int32[] localWorkSize)
        {
            return ExecuteInternal(commandQueue, Convert(globalWorkSize), Convert(localWorkSize));
        }
        public Event Execute(CommandQueue commandQueue, Int32[] globalWorkSize, Int32[] localWorkSize, Int32[] globalWorkOffset)
        {
            return ExecuteInternal(commandQueue, Convert(globalWorkSize), Convert(localWorkSize), null, Convert(globalWorkOffset));
        }
        public Event Execute(CommandQueue commandQueue, Int32[] globalWorkSize, Int32[] localWorkSize, Int32[] globalWorkOffset, Events eventWaitList)
        {
            return ExecuteInternal(commandQueue, Convert(globalWorkSize), Convert(localWorkSize), eventWaitList, Convert(globalWorkOffset));
        }
        public Event Execute(CommandQueue commandQueue, Int32[] globalWorkSize, Int32[] localWorkSize, Events eventWaitList)
        {
            return ExecuteInternal(commandQueue, Convert(globalWorkSize), Convert(localWorkSize), eventWaitList, null);
        }
        public Event Execute(CommandQueue commandQueue, Int32[] globalWorkSize, Int32[] localWorkSize, Events eventWaitList, Int32[] globalWorkOffset)
        {
            return ExecuteInternal(commandQueue, Convert(globalWorkSize), Convert(localWorkSize), eventWaitList, Convert(globalWorkOffset));
        }

        public Event Execute(CommandQueue commandQueue, UInt32 globalWorkSize)
        {
            return Execute(commandQueue, new UInt32[] { globalWorkSize });
        }
        public Event Execute(CommandQueue commandQueue, UInt32 globalWorkSize, UInt32 localWorkSize)
        {
            return Execute(commandQueue, new UInt32[] { globalWorkSize }, new UInt32[] { localWorkSize });
        }
        public Event Execute(CommandQueue commandQueue, UInt32[] globalWorkSize)
        {
            return ExecuteInternal(commandQueue, Convert(globalWorkSize));
        }
        public Event Execute(CommandQueue commandQueue, UInt32[] globalWorkSize, UInt32[] localWorkSize)
        {
            return ExecuteInternal(commandQueue, Convert(globalWorkSize), Convert(localWorkSize));
        }
        public Event Execute(CommandQueue commandQueue, UInt32[] globalWorkSize, UInt32[] localWorkSize, UInt32[] globalWorkOffset)
        {
            return ExecuteInternal(commandQueue, Convert(globalWorkSize), Convert(localWorkSize), null, Convert(globalWorkOffset));
        }
        public Event Execute(CommandQueue commandQueue, UInt32[] globalWorkSize, UInt32[] localWorkSize, UInt32[] globalWorkOffset, Events eventWaitList)
        {
            return ExecuteInternal(commandQueue, Convert(globalWorkSize), Convert(localWorkSize), eventWaitList, Convert(globalWorkOffset));
        }
        public Event Execute(CommandQueue commandQueue, UInt32[] globalWorkSize, UInt32[] localWorkSize, Events eventWaitList)
        {
            return ExecuteInternal(commandQueue, Convert(globalWorkSize), Convert(localWorkSize), eventWaitList, null);
        }
        public Event Execute(CommandQueue commandQueue, UInt32[] globalWorkSize, UInt32[] localWorkSize, Events eventWaitList, UInt32[] globalWorkOffset)
        {
            return ExecuteInternal(commandQueue, Convert(globalWorkSize), Convert(localWorkSize), eventWaitList, Convert(globalWorkOffset));
        }

        public Event Execute(CommandQueue commandQueue, Int64 globalWorkSize)
        {
            return Execute(commandQueue, new Int64[] { globalWorkSize });
        }
        public Event Execute(CommandQueue commandQueue, Int64 globalWorkSize, Int64 localWorkSize)
        {
            return Execute(commandQueue, new Int64[] { globalWorkSize }, new Int64[] { localWorkSize });
        }
        public Event Execute(CommandQueue commandQueue, Int64[] globalWorkSize)
        {
            return ExecuteInternal(commandQueue, Convert(globalWorkSize));
        }
        public Event Execute(CommandQueue commandQueue, Int64[] globalWorkSize, Int64[] localWorkSize)
        {
            return ExecuteInternal(commandQueue, Convert(globalWorkSize), Convert(localWorkSize));
        }
        public Event Execute(CommandQueue commandQueue, Int64[] globalWorkSize, Int64[] localWorkSize, Int64[] globalWorkOffset)
        {
            return ExecuteInternal(commandQueue, Convert(globalWorkSize), Convert(localWorkSize), null, Convert(globalWorkOffset));
        }
        public Event Execute(CommandQueue commandQueue, Int64[] globalWorkSize, Int64[] localWorkSize, Int64[] globalWorkOffset, Events eventWaitList)
        {
            return ExecuteInternal(commandQueue, Convert(globalWorkSize), Convert(localWorkSize), eventWaitList, Convert(globalWorkOffset));
        }
        public Event Execute(CommandQueue commandQueue, Int64[] globalWorkSize, Int64[] localWorkSize, Events eventWaitList)
        {
            return ExecuteInternal(commandQueue, Convert(globalWorkSize), Convert(localWorkSize), eventWaitList, null);
        }
        public Event Execute(CommandQueue commandQueue, Int64[] globalWorkSize, Int64[] localWorkSize, Events eventWaitList, Int64[] globalWorkOffset)
        {
            return ExecuteInternal(commandQueue, Convert(globalWorkSize), Convert(localWorkSize), eventWaitList, Convert(globalWorkOffset));
        }

        public Event Execute(CommandQueue commandQueue, UInt64 globalWorkSize)
        {
            return Execute(commandQueue, new UInt64[] { globalWorkSize });
        }
        public Event Execute(CommandQueue commandQueue, UInt64 globalWorkSize, UInt64 localWorkSize)
        {
            return Execute(commandQueue, new UInt64[] { globalWorkSize }, new UInt64[] { localWorkSize });
        }
        public Event Execute(CommandQueue commandQueue, UInt64[] globalWorkSize)
        {
            return ExecuteInternal(commandQueue, Convert(globalWorkSize));
        }
        public Event Execute(CommandQueue commandQueue, UInt64[] globalWorkSize, UInt64[] localWorkSize)
        {
            return ExecuteInternal(commandQueue, Convert(globalWorkSize), Convert(localWorkSize));
        }
        public Event Execute(CommandQueue commandQueue, UInt64[] globalWorkSize, UInt64[] localWorkSize, UInt64[] globalWorkOffset)
        {
            return ExecuteInternal(commandQueue, Convert(globalWorkSize), Convert(localWorkSize), null, Convert(globalWorkOffset));
        }
        public Event Execute(CommandQueue commandQueue, UInt64[] globalWorkSize, UInt64[] localWorkSize, UInt64[] globalWorkOffset, Events eventWaitList)
        {
            return ExecuteInternal(commandQueue, Convert(globalWorkSize), Convert(localWorkSize), eventWaitList, Convert(globalWorkOffset));
        }
        public Event Execute(CommandQueue commandQueue, UInt64[] globalWorkSize, UInt64[] localWorkSize, Events eventWaitList)
        {
            return ExecuteInternal(commandQueue, Convert(globalWorkSize), Convert(localWorkSize), eventWaitList, null);
        }
        public Event Execute(CommandQueue commandQueue, UInt64[] globalWorkSize, UInt64[] localWorkSize, Events eventWaitList, UInt64[] globalWorkOffset)
        {
            return ExecuteInternal(commandQueue, Convert(globalWorkSize), Convert(localWorkSize), eventWaitList, Convert(globalWorkOffset));
        }

        /* Static members */
        private static SizeT[] Convert<T>(T[] data)
        {
            if (data == null)
            {
                return null;
            }
            else
            {
                Int32 structureSize = Marshal.SizeOf(typeof(T));

                SizeT[] result = new SizeT[data.Length];

                GCHandle handle = GCHandle.Alloc(data, GCHandleType.Pinned);

                try
                {
                    IntPtr handlePtr = handle.AddrOfPinnedObject();

                    if (structureSize < IntPtr.Size)
                    {
                        for (int i = 0; i < result.Length; i++)
                        {
                            byte[] bytes = new byte[4];

                            for (int j = 0; j < structureSize; j++)
                            {
                                bytes[j] = Marshal.ReadByte(handlePtr, i * structureSize + j);
                            }

                            result[i] = new SizeT(BitConverter.ToInt32(bytes, 0));
                        }
                    }
                    else
                    {
                        for (int i = 0; i < result.Length; i++)
                        {
                            result[i] = new SizeT(Marshal.ReadIntPtr(handlePtr, structureSize * i).ToInt64());
                        }
                    }

                    return result;
                }
                finally
                {
                    handle.Free();
                }
            }
        }
        internal static T GetKernelInfo<T>(CLKernel clKernel, CLKernelInfo kernelInfo)
        {
            Byte[] buffer = GetKernelInfoBuffer(clKernel, kernelInfo);

            GCHandle bufferHandle = GCHandle.Alloc(buffer, GCHandleType.Pinned);

            try
            {
                return (T)Marshal.PtrToStructure(bufferHandle.AddrOfPinnedObject(), typeof(T));
            }
            finally
            {
                bufferHandle.Free();
            }
        }
        internal static String GetKernelInfoString(CLKernel clKernel, CLKernelInfo kernelInfo)
        {
            return GetKernelInfoString(clKernel, kernelInfo, Encoding.ASCII);
        }
        internal static String GetKernelInfoString(CLKernel clKernel, CLKernelInfo kernelInfo, Encoding encoding)
        {
            byte[] buffer = GetKernelInfoBuffer(clKernel, kernelInfo);

            Int32 count = Array.IndexOf<Byte>(buffer, 0, 0);

            return encoding.GetString(buffer, 0, count < 0 ? buffer.Length : count);
        }

        internal static Byte[] GetKernelInfoBuffer(CLKernel clKernel, CLKernelInfo kernelInfo)
        {
            SizeT kernelInfoBufferSizeOutput = SizeT.Zero;
            SizeT kernelInfoBufferSizeInput = GetKernelInfoBufferSize(clKernel, kernelInfo);

            Byte[] buffer = new Byte[kernelInfoBufferSizeInput];

            GCHandle bufferHandle = GCHandle.Alloc(buffer, GCHandleType.Pinned);

            IntPtr bufferPtr = bufferHandle.AddrOfPinnedObject();

            try
            {
                OpenCLError.Validate(OpenCLDriver.clGetKernelInfo(clKernel, kernelInfo, kernelInfoBufferSizeInput, bufferPtr, ref kernelInfoBufferSizeOutput));
            }
            finally
            {
                bufferHandle.Free();
            }

            Array.Resize(ref buffer, kernelInfoBufferSizeOutput);

            return buffer;
        }
        internal static SizeT GetKernelInfoBufferSize(CLKernel clKernel, CLKernelInfo kernelInfo)
        {
            SizeT bufferSize = SizeT.Zero;

            OpenCLError.Validate(OpenCLDriver.clGetKernelInfo(clKernel, kernelInfo, SizeT.Zero, IntPtr.Zero, ref bufferSize));

            return bufferSize;
        }

        public static Kernel Create(String name, Program program, params Argument[] arguments)
        {
            CLError error = CLError.None;

            CLKernel clKernel = OpenCLDriver.clCreateKernel(program.CLProgram, name, ref error);

            OpenCLError.Validate(error);

            Kernel result = new Kernel(clKernel, program, name);

            KernelArguments kernelArguments = result.Arguments;

            for (Int32 i = 0; i < arguments.Length; i++)
            {
                kernelArguments[i] = arguments[i];
            }

            return result;
        }

        /* Operators & miscellaneous members */
        public override Int32 GetHashCode()
        {
            return CLKernel.GetHashCode();
        }
        public override Boolean Equals(object obj)
        {
            return obj is Kernel && Object.Equals(((Kernel)(obj)).CLKernel, CLKernel);
        }

        public static Boolean operator ==(Kernel kernelA, Kernel kernelB)
        {
            if (Object.Equals(kernelA, null) || Object.Equals(kernelB, null))
            {
                return Object.Equals(kernelA, kernelB);
            }
            else
            {
                return Object.Equals(kernelA.CLKernel, kernelB.CLKernel);
            }
        }
        public static Boolean operator !=(Kernel kernelA, Kernel kernelB)
        {
            if (Object.Equals(kernelA, null) || Object.Equals(kernelB, null))
            {
                return !Object.Equals(kernelA, kernelB);
            }
            else
            {
                return !Object.Equals(kernelA.CLKernel, kernelB.CLKernel);
            }
        }

        #region IDisposable members
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                OpenCLError.Validate(OpenCLDriver.clReleaseKernel(CLKernel));

                disposed = true;
            }
        }
        #endregion

        ~Kernel()
        {
            Dispose(false);
        }
    }
}