namespace ManOCL.Internal
{
    using System;
    using System.Runtime.InteropServices;

    /// <summary>
    /// Used to represent a platform dependent sized variable.
    /// On 32 bit platforms it is 4 bytes wide (Int32, Int32), on 64 bit it is
    /// 8 bytes wide (Int64, UInt64).
    /// 
    /// This class maps to the C/C++ native size_t data type.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct SizeT
    {
        private IntPtr value;
        /// <summary>
        /// Creates a new instance based on the given value.
        /// </summary>
        /// <param name="value">Integer value to represent.</param>
        public SizeT(Int32 value)
        {
            this.value = new IntPtr(value);
        }

        /// <summary>
        /// Creates a new instance based on the given value.
        /// </summary>
        /// <param name="value">Integer value to represent.</param>
        public SizeT(UInt32 value)
        {
            this.value = new IntPtr((Int32) value);
        }

        /// <summary>
        /// Creates a new instance based on the given value.
        /// </summary>
        /// <param name="value">Integer value to represent.</param>
        public SizeT(Int64 value)
        {
            this.value = new IntPtr(value);
        }

        /// <summary>
        /// Creates a new instance based on the given value.
        /// </summary>
        /// <param name="value">Integer value to represent.</param>
        public SizeT(UInt64 value)
        {
            this.value = new IntPtr((Int64) value);
        }

        /// <summary>
        /// Converts the object to Int32.
        /// </summary>
        /// <param name="t">Object to convert.</param>
        /// <returns>Integer value represented by the object.</returns>
        public static implicit operator Int32(SizeT t)
        {
            return t.value.ToInt32();
        }

        /// <summary>
        /// Converts the object to Int32.
        /// </summary>
        /// <param name="t">Object to convert.</param>
        /// <returns>Integer value represented by the object.</returns>
        public static implicit operator UInt32(SizeT t)
        {
            return (UInt32) ((Int32) t.value);
        }

        /// <summary>
        /// Converts the object to Int64.
        /// </summary>
        /// <param name="t">Object to convert.</param>
        /// <returns>Integer value represented by the object.</returns>
        public static implicit operator Int64(SizeT t)
        {
            return t.value.ToInt64();
        }

        /// <summary>
        /// Converts the object to UInt64.
        /// </summary>
        /// <param name="t">Object to convert.</param>
        /// <returns>Integer value represented by the object.</returns>
        public static implicit operator UInt64(SizeT t)
        {
            return (UInt64) ((Int64) t.value);
        }

        /// <summary>
        /// Converts the given integer to an object.
        /// </summary>
        /// <param name="value">Integer value to convert.</param>
        /// <returns>New object representing this value.</returns>
        public static implicit operator SizeT(Int32 value)
        {
            return new SizeT(value);
        }

        /// <summary>
        /// Converts the given integer to an object.
        /// </summary>
        /// <param name="value">Integer value to convert.</param>
        /// <returns>New object representing this value.</returns>
        public static implicit operator SizeT(UInt32 value)
        {
            return new SizeT(value);
        }

        /// <summary>
        /// Converts the given integer to an object.
        /// </summary>
        /// <param name="value">Integer value to convert.</param>
        /// <returns>New object representing this value.</returns>
        public static implicit operator SizeT(Int64 value)
        {
            return new SizeT(value);
        }

        /// <summary>
        /// Converts the given integer to an object.
        /// </summary>
        /// <param name="value">Integer value to convert.</param>
        /// <returns>New object representing this value.</returns>
        public static implicit operator SizeT(UInt64 value)
        {
            return new SizeT(value);
        }

        /// <summary>
        /// Compares two SizeT objects.
        /// </summary>
        /// <param name="val1">First value to compare.</param>
        /// <param name="val2">Second value to compare.</param>
        /// <returns>true or false for the comparison result.</returns>
        public static bool operator !=(SizeT val1, SizeT val2)
        {
            return (val1.value != val2.value);
        }

        /// <summary>
        /// Compares two SizeT objects.
        /// </summary>
        /// <param name="val1">First value to compare.</param>
        /// <param name="val2">Second value to compare.</param>
        /// <returns>true or false for the comparison result.</returns>
        public static bool operator ==(SizeT val1, SizeT val2)
        {
            return (val1.value == val2.value);
        }

        /// <summary>
        /// Returns a value indicating whether this instance is equal to a specified object.
        /// </summary>
        /// <param name="obj">An object to compare with this instance or null.</param>
        /// <returns>true if obj is an instance of System.IntPtr and equals the value of this instance; otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            return this.value.Equals(obj);
        }

        /// <summary>
        /// Converts the numeric value of the current object to its equivalent string representation.
        /// </summary>
        /// <returns>The string representation of the value of this instance.</returns>
        public override string ToString()
        {
            return this.value.ToString();
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>A 32-bit signed integer hash code.</returns>
        public override Int32 GetHashCode()
        {
            return this.value.GetHashCode();
        }

        private static SizeT _Zero = new SizeT(0);

        public static SizeT Zero
        {
            get
            {
                return _Zero;
            }
        }
    }
}

