﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using ManOCL.Internal.OpenCL;
using ManOCL.Internal;

namespace ManOCL
{
    public class ArgumentLocalMemory : Argument
    {
        private SizeT size;

        internal ArgumentLocalMemory(SizeT size)
        {
            this.size = size;
        }

        public ArgumentLocalMemory SizeCreate(Int32 size)
        {
            return new ArgumentLocalMemory(size);
        }

        internal override void SetAsKernelArgument(CLKernel clKernel, Int32 index)
        {
            OpenCLError.Validate(OpenCLDriver.clSetKernelArg(clKernel, index, size, IntPtr.Zero));
        }
    }
}
