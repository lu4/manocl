﻿using System;
using System.Collections.Generic;
using System.Text;
using ManOCL.Internal.OpenCL;
using System.Runtime.InteropServices;
using System.IO;
using ManOCL.Internal;


namespace ManOCL
{
    public abstract class ArgumentBuffer : Argument, IDisposable
    {
        private Boolean disposed;

        internal delegate CLError TransferDelegate(CLCommandQueue commandQueue, CLMem buffer, CLBool blockingRead, SizeT offset, SizeT cb, IntPtr ptr, Int32 numEventsInWaitList, [In] CLEvent[] eventWaitList, ref CLEvent e);

        //private static Event Transfer(CLMem clMem, CommandQueue commandQueue, Array array, Int32 bytesToCopy, Int32 arrayOffset, Int32 arraySize, Int32 bufferOffset, Int32 bufferSize, Events eventWaitList, TransferDelegate transferDelegate)
        //{
        //    if (bufferSize < bufferOffset + bytesToCopy) throw new ArgumentException(Resources.Buffer_out_of_bounds);
        //    if (arraySize < arrayOffset + bytesToCopy) throw new ArgumentException(Resources.Array_out_of_bounds);

        //    GCHandle valueHandle = GCHandle.Alloc(array, GCHandleType.Pinned);

        //    try
        //    {
        //        CLEvent e = new CLEvent();

        //        unsafe
        //        {
        //            IntPtr valuePtr = new IntPtr((Byte*)(valueHandle.AddrOfPinnedObject().ToPointer()) + arrayOffset);

        //            OpenCLError.Validate
        //            (
        //                transferDelegate
        //                (
        //                    commandQueue.CLCommandQueue,
        //                    clMem,
        //                    CLBool.True,
        //                    new SizeT(bufferOffset),
        //                    new SizeT(bytesToCopy),
        //                    valuePtr,
        //                    eventWaitList == null ? 0 : eventWaitList.Count,
        //                    eventWaitList == null ? null : eventWaitList.OpenCLEventArray,
        //                    ref e
        //                )
        //            );
        //        }

        //        return new Event(e);
        //    }
        //    finally
        //    {
        //        valueHandle.Free();
        //    }
        //}
        private static Event Transfer(CLMem clMem, CommandQueue commandQueue, Array array, Int64 bytesToCopy, Int64 arrayOffset, Int64 arraySize, Int64 bufferOffset, Int64 bufferSize, Events eventWaitList, TransferDelegate transferDelegate)
        {
            if (bufferSize < bufferOffset + bytesToCopy) throw new ArgumentException(Resources.Buffer_out_of_bounds);
            if (arraySize < arrayOffset + bytesToCopy) throw new ArgumentException(Resources.Array_out_of_bounds);

            GCHandle valueHandle = GCHandle.Alloc(array, GCHandleType.Pinned);

            try
            {
                CLEvent e = new CLEvent();

                unsafe
                {
                    IntPtr valuePtr = new IntPtr((Byte*)(valueHandle.AddrOfPinnedObject().ToPointer()) + arrayOffset);

                    OpenCLError.Validate
                    (
                        transferDelegate
                        (
                            commandQueue.CLCommandQueue,
                            clMem,
                            CLBool.True,
                            new SizeT(bufferOffset),
                            new SizeT(bytesToCopy),
                            valuePtr,
                            eventWaitList == null ? 0 : eventWaitList.Count,
                            eventWaitList == null ? null : eventWaitList.OpenCLEventArray,
                            ref e
                        )
                    );
                }

                return new Event(e);
            }
            finally
            {
                valueHandle.Free();
            }
        }

        internal static CLMem CreateCLMem<T>(T data, SizeT dataSize, CLMemFlags memFlags, Context context, ArgumentBufferAccess access)
        {
            CLError error = CLError.None;

            GCHandle handle = GCHandle.Alloc(data, GCHandleType.Pinned);

            try
            {
                CLMem result = OpenCLDriver.clCreateBuffer(context.CLContext, memFlags, dataSize, handle.AddrOfPinnedObject(), ref error);

                OpenCLError.Validate(error);

                return result;
            }
            finally
            {
                handle.Free();
            }
        }

        private CLMem clMem;
        private SizeT clMemSize;

        internal ArgumentBuffer(CLMem clMem, SizeT sizeInternal, Context context, ArgumentBufferAccess access)
        {
            this.Access = access;
            this.Context = context;
            this.clMem = clMem;
            this.clMemSize = sizeInternal;
        }

        internal override void SetAsKernelArgument(CLKernel kernel, Int32 index)
        {
            OpenCLError.Validate(OpenCLDriver.clSetKernelArg(kernel, index, new SizeT(IntPtr.Size), ref clMem));
        }

        public Int32 Size
        {
            get
            {
                return (Int32)clMemSize;
            }
        }

        public Context Context { get; private set; }
        public ArgumentBufferAccess Access { get; private set; }

        public Event TransferTo(CommandQueue commandQueue, Array array, Int32 arrayOffset, Int32 bufferOffset, Int32 bytesToCopy)
        {
            Int32 arraySize = array.Length * Marshal.SizeOf(array.GetType().GetElementType());

            return Transfer(clMem, commandQueue, array, bytesToCopy, arrayOffset, arraySize, bufferOffset, clMemSize, null, OpenCLDriver.clEnqueueReadBuffer);
        }
        public Event TransferTo(CommandQueue commandQueue, Array array, Int32 arrayOffset, Int32 bufferOffset, Int32 bytesToCopy, Events eventWaitList)
        {
            Int32 arraySize = array.Length * Marshal.SizeOf(array.GetType().GetElementType());

            return Transfer(clMem, commandQueue, array, bytesToCopy, arrayOffset, arraySize, bufferOffset, clMemSize, eventWaitList, OpenCLDriver.clEnqueueReadBuffer);
        }

        public Event TransferFrom(CommandQueue commandQueue, Array array, Int32 arrayOffset, Int32 bufferOffset, Int32 bytesToCopy)
        {
            Int32 arraySize = array.Length * Marshal.SizeOf(array.GetType().GetElementType());

            return Transfer(clMem, commandQueue, array, bytesToCopy, arrayOffset, arraySize, bufferOffset, clMemSize, null, OpenCLDriver.clEnqueueWriteBuffer);
        }
        public Event TransferFrom(CommandQueue commandQueue, Array array, Int32 arrayOffset, Int32 bufferOffset, Int32 bytesToCopy, Events eventWaitList)
        {
            Int32 arraySize = array.Length * Marshal.SizeOf(array.GetType().GetElementType());

            return Transfer(clMem, commandQueue, array, bytesToCopy, arrayOffset, arraySize, bufferOffset, clMemSize, eventWaitList, OpenCLDriver.clEnqueueWriteBuffer);
        }

        public void TransferTo(CommandQueue commandQueue, Stream stream, Int32 bufferOffset, Int32 bytesToCopy, Byte[] buffer)
        {
            TransferTo(commandQueue, stream, bufferOffset, bytesToCopy, buffer, null);
        }
        public void TransferTo(CommandQueue commandQueue, Stream stream, Int32 bufferOffset, Int32 bytesToCopy, Byte[] buffer, Events eventWaitList)
        {
            while (bytesToCopy > buffer.Length)
            {
                TransferTo(commandQueue, buffer, 0, bufferOffset, buffer.Length, eventWaitList);

                stream.Write(buffer, 0, buffer.Length);

                bufferOffset += buffer.Length;
                bytesToCopy -= buffer.Length;
            }

            TransferTo(commandQueue, buffer, 0, bufferOffset, bytesToCopy, eventWaitList);

            stream.Write(buffer, 0, (Int32)bytesToCopy);
        }

        public void TransferFrom(CommandQueue commandQueue, Stream stream, Int32 bufferOffset, Int32 bytesToCopy, Byte[] buffer)
        {
            TransferFrom(commandQueue, stream, bufferOffset, bytesToCopy, buffer, null);
        }
        public void TransferFrom(CommandQueue commandQueue, Stream stream, Int32 bufferOffset, Int32 bytesToCopy, Byte[] buffer, Events eventWaitList)
        {
            while (bytesToCopy > buffer.Length)
            {
                stream.Read(buffer, 0, buffer.Length);

                TransferFrom(commandQueue, buffer, 0, bufferOffset, buffer.Length, eventWaitList);

                bufferOffset += buffer.Length;
                bytesToCopy -= buffer.Length;
            }

            stream.Read(buffer, 0, (Int32)bytesToCopy);

            TransferFrom(commandQueue, buffer, 0, bufferOffset, bytesToCopy, eventWaitList);
        }

        public Event TransferTo(CommandQueue commandQueue, Array array, Int64 arrayOffset, Int64 bufferOffset, Int64 bytesToCopy)
        {
            Int64 arraySize = array.Length * Marshal.SizeOf(array.GetType().GetElementType());

            return Transfer(clMem, commandQueue, array, bytesToCopy, arrayOffset, arraySize, bufferOffset, clMemSize, null, OpenCLDriver.clEnqueueReadBuffer);
        }
        public Event TransferTo(CommandQueue commandQueue, Array array, Int64 arrayOffset, Int64 bufferOffset, Int64 bytesToCopy, Events eventWaitList)
        {
            Int64 arraySize = array.Length * Marshal.SizeOf(array.GetType().GetElementType());

            return Transfer(clMem, commandQueue, array, bytesToCopy, arrayOffset, arraySize, bufferOffset, clMemSize, eventWaitList, OpenCLDriver.clEnqueueReadBuffer);
        }

        public Event TransferFrom(CommandQueue commandQueue, Array array, Int64 arrayOffset, Int64 bufferOffset, Int64 bytesToCopy)
        {
            Int64 arraySize = array.Length * Marshal.SizeOf(array.GetType().GetElementType());

            return Transfer(clMem, commandQueue, array, bytesToCopy, arrayOffset, arraySize, bufferOffset, clMemSize, null, OpenCLDriver.clEnqueueWriteBuffer);
        }
        public Event TransferFrom(CommandQueue commandQueue, Array array, Int64 arrayOffset, Int64 bufferOffset, Int64 bytesToCopy, Events eventWaitList)
        {
            Int64 arraySize = array.Length * Marshal.SizeOf(array.GetType().GetElementType());

            return Transfer(clMem, commandQueue, array, bytesToCopy, arrayOffset, arraySize, bufferOffset, clMemSize, eventWaitList, OpenCLDriver.clEnqueueWriteBuffer);
        }

        public void TransferTo(CommandQueue commandQueue, Stream stream, Int64 bufferOffset, Int64 bytesToCopy, Byte[] buffer)
        {
            TransferTo(commandQueue, stream, bufferOffset, bytesToCopy, buffer, null);
        }
        public void TransferTo(CommandQueue commandQueue, Stream stream, Int64 bufferOffset, Int64 bytesToCopy, Byte[] buffer, Events eventWaitList)
        {
            while (bytesToCopy > buffer.Length)
            {
                TransferTo(commandQueue, buffer, 0, bufferOffset, buffer.Length, eventWaitList);

                stream.Write(buffer, 0, buffer.Length);

                bufferOffset += buffer.Length;
                bytesToCopy -= buffer.Length;
            }

            TransferTo(commandQueue, buffer, 0, bufferOffset, bytesToCopy, eventWaitList);

            stream.Write(buffer, 0, (Int32)bytesToCopy);
        }

        public void TransferFrom(CommandQueue commandQueue, Stream stream, Int64 bufferOffset, Int64 bytesToCopy, Byte[] buffer)
        {
            TransferFrom(commandQueue, stream, bufferOffset, bytesToCopy, buffer, null);
        }
        public void TransferFrom(CommandQueue commandQueue, Stream stream, Int64 bufferOffset, Int64 bytesToCopy, Byte[] buffer, Events eventWaitList)
        {
            while (bytesToCopy > buffer.Length)
            {
                stream.Read(buffer, 0, buffer.Length);

                TransferFrom(commandQueue, buffer, 0, bufferOffset, buffer.Length, eventWaitList);

                bufferOffset += buffer.Length;
                bytesToCopy -= buffer.Length;
            }

            stream.Read(buffer, 0, (Int32)bytesToCopy);

            TransferFrom(commandQueue, buffer, 0, bufferOffset, bytesToCopy, eventWaitList);
        }

        #region IDisposable members
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                OpenCLError.Validate(OpenCLDriver.clReleaseMemObject(clMem));

                disposed = true;
            }
        }
        #endregion

        ~ArgumentBuffer()
        {
            Dispose(false);
        }
    }
}