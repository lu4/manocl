﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManOCL
{
    public class ContextLogEventArgs : EventArgs
    {
        public String Message { get; private set; }
    }
}
