﻿using System;
using System.Collections.Generic;
using System.Text;
using ManOCL.Internal.OpenCL;
using System.Runtime.InteropServices;
using ManOCL.Internal;


namespace ManOCL
{
    public partial class Context : IDisposable
    {
        private Boolean disposed;

        public static Context Create(Platforms platforms, Devices devices)
        {
            Int32 count = platforms.Count;
            IntPtr[] properties = new IntPtr[2 * platforms.Count + 1];

            for (Int32 i = 0; i < count; i++)
            {
                properties[2 * i] = new IntPtr((Int32)CLContextProperties.Platform);
                properties[2 * i + 1] = platforms[i].CLPlatformID.Value;
            }

            Context context = new Context(platforms, devices);

            CLError error = CLError.None;

            context.CLContext = OpenCLDriver.clCreateContext(properties, devices.Count, devices.OpenCLDeviceArray, context.LoggingFunction, IntPtr.Zero, ref error);

            OpenCLError.Validate(error);

            return context;
        }
        public static Context Create(Platforms platforms, DeviceType deviceType)
        {
            return Create(platforms, Devices.Create(deviceType, platforms));
        }
        public static Context ShareWithCGL(IntPtr cglShareGroup)
        {
            IntPtr[] properties = 
            {
                new IntPtr(0x10000000), // CL_CONTEXT_PROPERTY_USE_CGL_SHAREGROUP_APPLE
                cglShareGroup,
                new IntPtr(0)
            };

            CLError error = CLError.None;

            // TODO: Add parameter pfn_notify (logging function)
            CLContext clContext = OpenCLDriver.clCreateContext(properties, 0, null, null, IntPtr.Zero, ref error);

            OpenCLError.Validate(error);

            SizeT devicesSize = SizeT.Zero;

            OpenCLError.Validate(OpenCLDriver.clGetContextInfo(clContext, CLContextInfo.Devices, SizeT.Zero, IntPtr.Zero, ref devicesSize));

            CLDeviceID[] devices = new CLDeviceID[devicesSize / IntPtr.Size];

            GCHandle devicesHandle = GCHandle.Alloc(devices, GCHandleType.Pinned);

            try
            {
                OpenCLError.Validate(OpenCLDriver.clGetContextInfo(clContext, CLContextInfo.Devices, devicesSize, devicesHandle.AddrOfPinnedObject(), ref devicesSize));

                Dictionary<CLPlatformID, CLPlatformID> platformsDictionary = new Dictionary<CLPlatformID, CLPlatformID>();

                foreach (CLDeviceID device in devices)
                {
                    SizeT platformsSize = SizeT.Zero;

                    OpenCLError.Validate(OpenCLDriver.clGetDeviceInfo(device, CLDeviceInfo.Platform, SizeT.Zero, IntPtr.Zero, ref platformsSize));

                    CLPlatformID[] platforms = new CLPlatformID[((Int64)(platformsSize)) / IntPtr.Size];

                    GCHandle platformsHandle = GCHandle.Alloc(platforms, GCHandleType.Pinned);

                    try
                    {
                        OpenCLError.Validate(OpenCLDriver.clGetDeviceInfo(device, CLDeviceInfo.Platform, platformsSize, platformsHandle.AddrOfPinnedObject(), ref platformsSize));

                        foreach (CLPlatformID platform in platforms)
                        {
                            if (!platformsDictionary.ContainsKey(platform))
                            {
                                platformsDictionary.Add(platform, platform);
                            }
                        }
                    }
                    finally
                    {
                        platformsHandle.Free();
                    }
                }

                Int32 index = 0;

                CLPlatformID[] result = new CLPlatformID[platformsDictionary.Count];

                foreach (var platform in platformsDictionary.Keys)
                {
                    result[index++] = platform;
                }

                return new Context(new Platforms(result), new Devices(devices), clContext);
            }
            finally
            {
                devicesHandle.Free();
            }
        }

        private Context(Platforms platforms, Devices devices)
            : this(platforms, devices, CLContext.Zero)
        {
        }
        private Context(Platforms platforms, Devices devices, CLContext clContext)
        {
            this.Devices = devices;
            this.Platforms = platforms;
            this.CLContext = clContext;
        }


        internal CLContext CLContext { get; private set; }
        internal String ToIndentedString(Int32 ident, Int32 identSize)
        {
            String identation = new String(' ', identSize * ident);

            StringBuilder sb = new StringBuilder();

            sb.AppendLine(identation + "ContextFactory");
            sb.AppendLine(identation + "{");
            sb.AppendLine(Platforms.ToIdentedString(ident + 1, identSize));
            sb.AppendLine(Devices.ToIdentedString(ident + 1, identSize));
            sb.AppendLine(identation + "}");

            return sb.ToString();
        }

        private void LoggingFunction(IntPtr errinfo, IntPtr private_info, SizeT cb, IntPtr user_data)
        {
            if (LogEvent != null)
            {
                throw new NotImplementedException();
                LogEvent(this, new ContextLogEventArgs());
            }
        }

        public Devices Devices { get; private set; }
        public Platforms Platforms { get; private set; }

        //#region public static Context Default { get; } /* Singleton */
        //private static Context _Default;
        //public static Context Default
        //{
        //    get
        //    {
        //        if (_Default == null)
        //        {
        //            Platforms platforms = Platforms.Create();
        //            Devices devices = Devices.Create(DeviceType.All, platforms);

        //            _Default = Create(platforms, devices);
        //        }

        //        return _Default;
        //    }
        //    set
        //    {
        //        _Default = value;
        //    }
        //}
        //#endregion

        public void Dispose()
        {
            if (!disposed)
            {
                OpenCLError.Validate(OpenCLDriver.clReleaseContext(CLContext));

                disposed = true;
            }
        }

        public override string ToString()
        {
            return ToIndentedString(0, Globals.IdentSize);
        }

        public event EventHandler<ContextLogEventArgs> LogEvent;
    }
}