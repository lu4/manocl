using System;

using ManOCL.Internal.OpenCL;
using ManOCL.Internal;


namespace ManOCL
{
    public class DeviceSampler : Argument
    {
        CLSampler clSampler;

        //public const Boolean DefaultNormalizedCoords = false;
        //public const FilterMode DefaultFilterMode = FilterMode.Nearest;
        //public const AddressingMode DefaultAddressingMode = AddressingMode.ClampToEdge;

        public static DeviceSampler Create(Context context, Boolean normalizedCoords, AddressingMode addressingMode, FilterMode filterMode)
        {
            CLError error = new CLError();

            CLSampler sampler = OpenCLDriver.clCreateSampler(context.CLContext, normalizedCoords ? CLBool.True : CLBool.False, (CLAddressingMode)addressingMode, (CLFilterMode)filterMode, ref error);

            OpenCLError.Validate(error);

            return new DeviceSampler()
            {
                clSampler = sampler
            };
        }
        //public static DeviceSampler Create(Context context, Boolean normalizedCoords, AddressingMode addressingMode)
        //{
        //    return Create(context, normalizedCoords, addressingMode, DefaultFilterMode);
        //}
        //public static DeviceSampler Create(Context context, AddressingMode addressingMode, FilterMode filterMode)
        //{
        //    return Create(context, false, addressingMode, filterMode);
        //}
        //public static DeviceSampler Create(Context context, Boolean normalizedCoords)
        //{
        //    return Create(context, normalizedCoords, DefaultAddressingMode, DefaultFilterMode);
        //}
        //public static DeviceSampler Create(Context context, FilterMode filterMode)
        //{
        //    return Create(context, DefaultNormalizedCoords, DefaultAddressingMode, filterMode);
        //}
        //public static DeviceSampler Create(Context context, AddressingMode addressingMode)
        //{
        //    return Create(context, DefaultNormalizedCoords, addressingMode, DefaultFilterMode);
        //}
        //public static DeviceSampler Create(Context context)
        //{
        //    return Create(context, DefaultNormalizedCoords, DefaultAddressingMode, DefaultFilterMode);
        //}

        internal override void SetAsKernelArgument(CLKernel kernel, Int32 index)
        {
            var clMem = new CLMem(clSampler.Value);

            OpenCLError.Validate(OpenCLDriver.clSetKernelArg(kernel, index, new SizeT(IntPtr.Size), ref clMem));
        }

    }
}