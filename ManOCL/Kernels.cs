﻿using System;
using System.Collections.Generic;
using System.Text;
using ManOCL.Internal.OpenCL;
using System.Collections;

namespace ManOCL
{
    public class Kernels : IEnumerable<Kernel>
    {
        private static Kernel[] CreateKernels(Program program)
        {
            Int32 numKernels = 0;

            CLKernel[] clKernels = null;

            OpenCLError.Validate(OpenCLDriver.clCreateKernelsInProgram(program.CLProgram, 0, clKernels, ref numKernels));

            clKernels = new CLKernel[numKernels];

            OpenCLError.Validate(OpenCLDriver.clCreateKernelsInProgram(program.CLProgram, numKernels, clKernels, ref numKernels));

            Kernel[] result = new Kernel[numKernels];

            for (Int32 i = 0; i < numKernels; i++)
            {
                var clKernel = clKernels[i];

                result[i] = new Kernel(clKernel, program);
            }
            return result;
        }

        private Kernel[] kernelsArray;
        private Dictionary<Kernel, Kernel> kernelSetDictionary;
        private Dictionary<String, Kernel> kernelNamesDictionary;

        public Kernels(Kernel[] kernels)
        {
            this.kernelsArray = (Kernel[])kernels.Clone();
			this.kernelSetDictionary = new Dictionary<Kernel, Kernel>(kernels.Length);
            this.kernelNamesDictionary = new Dictionary<String, Kernel>(kernels.Length);

            foreach (Kernel kernel in kernels)
            {
                if (!kernelSetDictionary.ContainsKey(kernel))
                {
                    kernelSetDictionary.Add(kernel, kernel);
                }

                if (!kernelNamesDictionary.ContainsKey(kernel.Name))
                {
                    kernelNamesDictionary.Add(kernel.Name, kernel);
                }
            }
        }
        public Kernels(Program program)
            : this(CreateKernels(program))
        {
        }

        public Kernels(Program program, IEnumerable<KeyValuePair<String, Argument[]>> arguments)
            : this(program)
        {
            foreach (KeyValuePair<String, Argument[]> item in arguments)
            {
                Add(item.Key, item.Value);
            }
        }

        public Kernels(Program program, params KeyValuePair<String, Argument[]>[] arguments)
            : this(program, (IEnumerable<KeyValuePair<String, Argument[]>>)(arguments))
        {

        }

        public void Add(String kernelName, params Argument[] arguments)
        {
            Kernel kernel = TryGetName(kernelName);

            if (kernel != null)
            {
                KernelArguments kernelArguments = kernel.Arguments;

                if (arguments.Length == kernelArguments.Count)
                {
                    for (Int32 i = 0; i < arguments.Length; i++)
                    {
                        kernelArguments[i] = arguments[i];
                    }
                }
                else
                {
                    throw new RankException();
                }
            }
            else
            {
                throw new IndexOutOfRangeException();
            }
        }

        public Int32 Count
        {
            get
            {
                return kernelsArray.Length;
            }
        }

        public Kernel this[Int32 index]
        {
            get
            {
                return kernelsArray[index];
            }
        }
        public Kernel this[String name]
        {
            get
            {
                return kernelNamesDictionary[name];
            }
        }

        public Kernel TryGetName(String name)
        {
            Kernel result;

            kernelNamesDictionary.TryGetValue(name, out result);

            return result;
        }

        public Boolean Contains(Kernel kernel)
        {
            return kernelSetDictionary.ContainsKey(kernel);
        }
        public Boolean ContainsName(String name)
        {
            return kernelNamesDictionary.ContainsKey(name);
        }

        public IEnumerator<Kernel> GetEnumerator()
        {
            foreach (Kernel kernel in kernelsArray)
            {
                yield return kernel;
            }
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            foreach (Kernel kernel in kernelsArray)
            {
                yield return kernel;
            }
        }
    }
}
