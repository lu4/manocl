﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using ManOCL;
using NUnit.Framework;

namespace UnitTests
{
    [TestFixture]
    public class TestFixture
    {
        private const String SimpleTestKernel =
        @"
            __kernel void Kernel(__global float *input, __global float *output)
            {
                output[get_global_id(0)] = input[get_global_id(0)] * 2.0F;
            }
        ";

        [Test]
        public void SimpleTest()
        {
            var platforms = Platforms.Create();
            var devices = Devices.Create(DeviceType.All, platforms);
            var context = Context.Create(platforms, devices);
            var program = Program.Create(context, devices, SimpleTestKernel);

            if (program.Build())
            {
                var commandQueue = CommandQueue.Create
                (
                    context,
                    devices.Where
                    (
                        device => device.Name == "GeForce 9400M"
                    )
                    .First(),
                    CommandQueueProperties.ProfilingEnable
                );

                var input = new Single[16];
                var output = new Single[input.Length];

                for (int i = 0; i < input.Length; i++)
                {
                    input[i] = i;
                }

                var inputArg = ArgumentGlobalMemory.ArrayCreate(context, ArgumentBufferAccess.ReadWrite, input);
                var outputArg = ArgumentGlobalMemory.ArrayCreate(context, ArgumentBufferAccess.ReadWrite, input);

                var kernels = new Kernels(program)
                {
                    {"Kernel", new Argument[] { inputArg, outputArg }}
                };

                kernels["Kernel"].Execute(commandQueue, input.Length);

                commandQueue.Finish();

                outputArg.TransferTo(commandQueue, output, 0, 0, outputArg.Size);

                for (int i = 0; i < output.Length; i++)
                {
                    Console.Write("{0} ", output[i]);
                    Assert.IsTrue(input[i] * 2 == output[i]);
                }
            }
            else
            {
                Console.WriteLine(program.GetBuildStatus(devices[0]));
            }
        }
        [Test]
        public void LoggingFunctionTest()
        {
            var platforms = Platforms.Create();
            var devices = Devices.Create(DeviceType.All, platforms);
            var context = Context.Create(platforms, devices);
            var program = Program.Create(context, devices, SimpleTestKernel);

            context.LogEvent += new EventHandler<ContextLogEventArgs>
            (
                delegate(object sender, ContextLogEventArgs e)
                {
                    throw new NotImplementedException();
                }
            );

            if (program.Build())
            {
                var commandQueue = CommandQueue.Create
                (
                    context,
                    devices.Where
                    (
                        device => device.Name == "GeForce 9400M"
                    )
                    .First(),
                    CommandQueueProperties.ProfilingEnable
                );

                var input = new Single[16];
                var output = new Single[input.Length];

                for (int i = 0; i < input.Length; i++)
                {
                    input[i] = i;
                }

                var inputArg = ArgumentGlobalMemory.ArrayCreate(context, ArgumentBufferAccess.ReadWrite, input);
                var outputArg = ArgumentGlobalMemory.ArrayCreate(context, ArgumentBufferAccess.ReadWrite, input);

                var kernels = new Kernels(program)
                {
                    {"Kernel", new Argument[] { inputArg, outputArg }}
                };

                kernels["Kernel"].Execute(commandQueue, input.Length * 2);

                commandQueue.Finish();

                outputArg.TransferTo(commandQueue, output, 0, 0, outputArg.Size);

                for (int i = 0; i < output.Length; i++)
                {
                    Console.Write("{0} ", output[i]);
                    Assert.IsTrue(input[i] * 2 == output[i]);
                }
            }
            else
            {
                Console.WriteLine(program.GetBuildStatus(devices[0]));
            }
        }

        //[Test]
        //public void TestFault()
        //{
        //    Assert.IsTrue(false);
        //}
    }
}