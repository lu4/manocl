﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnitTests;

namespace Debug
{
    class Program
    {
        static void Main(string[] args)
        {
            var unitTests = new TestFixture();

            unitTests.LoggingFunctionTest();
        }
    }
}
